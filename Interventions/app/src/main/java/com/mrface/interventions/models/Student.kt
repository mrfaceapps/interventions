package com.mrface.interventions.models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

@RealmClass
open class Student : RealmObject() {
    @PrimaryKey
    var id = ""
    var firstname: String = ""
    var lastname: String = ""
    var dob: Date? = null
    var tags: RealmList<String> = RealmList()
    var lucid: String = ""
    var rapid: String = ""
    var pupilPremium: Boolean = false

    var quickDate: String = ""
    var quickPuma: String = ""
    var quickPira: String = ""
    var quickReadingAge: String = ""
    var quickWriting: String = ""

    fun toJson(): JSONObject {
        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        var obj = JSONObject()

        obj.put("id",id)
        obj.put("firstname",firstname)
        obj.put("lastname",lastname)
        obj.put("dob",if (dob != null) dateFormatter.format(dob) else "")
        obj.put("lucid",lucid)
        obj.put("rapid",rapid)
        obj.put("pupilPremium",pupilPremium)
        obj.put("quickDate",quickDate)
        obj.put("quickPuma",quickPuma)
        obj.put("quickPira",quickPira)
        obj.put("quickReadingAge",quickReadingAge)
        obj.put("quickWriting",quickWriting)

        return obj
    }

    fun fromJson(jsonObject: JSONObject) {

        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

        firstname = jsonObject.getString("firstname")
        lastname = jsonObject.getString("lastname")
        dob = if(jsonObject.getString("dob") != "") dateFormatter.parse(jsonObject.getString("dob")) else null
        lucid = jsonObject.getString("lucid")
        rapid = jsonObject.getString("rapid")
        pupilPremium = jsonObject.getBoolean("pupilPremium")
        quickDate = jsonObject.getString("quickDate")
        quickPuma = jsonObject.getString("quickPuma")
        quickPira = jsonObject.getString("quickPira")
        quickReadingAge = jsonObject.getString("quickReadingAge")
        quickWriting = jsonObject.getString("quickWriting")
    }
}