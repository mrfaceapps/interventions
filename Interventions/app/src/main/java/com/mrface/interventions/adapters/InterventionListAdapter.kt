package com.mrface.interventions.adapters

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.InterventionItemClickListener
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.Utils.px
import com.mrface.interventions.models.Intervention
import com.mrface.interventions.models.InterventionSession
import kotlinx.android.synthetic.main.rv_intervention_item.view.*

class InterventionListAdapter(val items: ArrayList<Intervention>, val sessions: ArrayList<InterventionSession>, val context: Context, val listener: StudentItemClickListener, val interventionListener: InterventionItemClickListener) : RecyclerView.Adapter<ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.nameTV.text = items.get(p1).name
        p0.nameTV.typeface = Typeface.DEFAULT_BOLD
        var currentInterventionSessions = ArrayList<InterventionSession>()
        for(session in sessions){
            if(session.intervention?.id == items.get(p1).id && !currentInterventionSessions.contains(session) && (session.student != null && currentInterventionSessions.filter { it.student == session.student }.isEmpty())) {
                currentInterventionSessions.add(session)
            }
        }
        currentInterventionSessions = ArrayList(currentInterventionSessions.sortedBy { it.student?.lastname })
        p0.studentsRV.adapter = InterventionStudentSessionAdapter(currentInterventionSessions,context,p0.itemView,listener)
        p0.studentsRV.layoutManager = LinearLayoutManager(context)

        val border = GradientDrawable()
        border.cornerRadius = 20.px.toFloat()
        border.setColor(ContextCompat.getColor(context,R.color.LightGrey))
        border.setStroke(5.px,context.resources.getIntArray(R.array.colors)[items.get(p1).colorIndex])
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
            p0.itemView.setBackgroundDrawable(border)
        }else{
            p0.itemView.background = border
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_intervention_item,p0,false)).listen { position, type ->
            interventionListener.onClick(items.get(position))
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
    val nameTV = view.rv_intervention_item_name_tv
    val studentsRV = view.rv_intervention_item_RV


}
