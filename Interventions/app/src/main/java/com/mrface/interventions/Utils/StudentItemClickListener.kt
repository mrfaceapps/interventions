package com.mrface.interventions.Utils

import com.mrface.interventions.models.Student

interface StudentItemClickListener {
    fun onClick(item : Student)
}