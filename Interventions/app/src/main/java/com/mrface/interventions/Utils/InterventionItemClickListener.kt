package com.mrface.interventions.Utils

import com.mrface.interventions.models.Intervention

interface InterventionItemClickListener {
    fun onClick(item: Intervention)
}