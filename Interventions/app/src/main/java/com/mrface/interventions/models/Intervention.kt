package com.mrface.interventions.models

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

@RealmClass
open class Intervention : RealmObject() {
    @PrimaryKey
    var id = ""
    var name: String = ""
    var comments:String = ""
    var colorIndex: Int = 0
    var deletedActive: Boolean = false
    var deletedInactive: Boolean = false

    fun toJson(): JSONObject {
        var obj = JSONObject()

        obj.put("id",id);
        obj.put("name",name);
        obj.put("comments",comments);
        obj.put("colorIndex",colorIndex);
        obj.put("deletedActive",deletedActive);
        obj.put("deletedInactive",deletedInactive);

        return obj
    }

    fun fromJson(jsonObject: JSONObject) {

        name = jsonObject.getString("name")
        comments = jsonObject.getString("comments")
        colorIndex = jsonObject.getInt("colorIndex")
        deletedActive = jsonObject.getBoolean("deletedActive")
        deletedInactive = jsonObject.getBoolean("deletedInactive")
    }
}