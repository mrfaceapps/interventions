package com.mrface.interventions.adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import kotlinx.android.synthetic.main.rv_student_brief.view.*
import kotlin.math.round

class StudentListAdapter(val items: ArrayList<Student>, val context: Context, val listener: StudentItemClickListener) : RecyclerView.Adapter<StudentViewHolder>() {
    override fun onBindViewHolder(p0: StudentViewHolder, p1: Int) {
        var student1 = items.get(p1)

        p0.nameTV.text = student1.firstname + " " + student1.lastname
        p0.nameTV.setOnClickListener {
            listener.onClick(items.get(p1))
        }
        if((itemCount + p1) < items.size){
            val student2 = items.get(p1 + itemCount)
            p0.tags.text = student2.firstname + " " + student2.lastname
            p0.tags.setOnClickListener {
                listener.onClick(items.get(p1 + itemCount))
            }
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): StudentViewHolder {

        return StudentViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_student_brief,p0,false)).listen { pos, type ->
            listener.onClick(items.get(pos))
        }
    }

    override fun getItemCount(): Int {
        if(items.size % 2 == 0){
            return items.size / 2
        }
        return (items.size + 1) / 2
    }

}

class StudentViewHolder (view: View) : RecyclerView.ViewHolder(view){
    var nameTV = view.rv_student_brief_name
    var tags = view.rv_student_brief_tags

}