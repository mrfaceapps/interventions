package com.mrface.interventions.models

import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.kotlin.where
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

@RealmClass
open class InterventionSession : RealmObject() {
    @PrimaryKey
    var id = ""
    var student: Student? = null
    var intervention: Intervention? = null
    var startDate: Date? = null
    var completeDate: Date? = null
    var finished: Boolean = false
    var startingPoint: String = ""
    var finishingPoint: String = ""
    var frequency: String = ""
    var comments : RealmList<Comments> = RealmList()

    fun toJson(): JSONObject {
        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        var obj = JSONObject()

        obj.put("id",id);
        obj.put("student",if(student != null) student!!.id else "");
        obj.put("intervention",if(intervention != null) intervention!!.id else "");
        obj.put("startDate",if(startDate != null) dateFormatter.format(startDate) else "");
        obj.put("completeDate",if(completeDate != null) dateFormatter.format(completeDate) else "");
        obj.put("finished",finished);
        obj.put("startingPoint",startingPoint);
        obj.put("finishingPoint",finishingPoint);
        obj.put("frequency",frequency);
        if(comments.size > 0){
            var commentsArr = JSONArray()
            for (comment in comments){
                commentsArr.put(comment.id)
            }
            obj.put("comments",commentsArr)
        }

        return obj
    }

    fun fromJson(jsonObject: JSONObject) {

        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

        if(jsonObject.getString("student") != ""){
            student = Realm.getDefaultInstance().where<Student>().equalTo("id",jsonObject.getString("student")).findFirst()
        }
        if(jsonObject.getString("intervention") != ""){
            intervention = Realm.getDefaultInstance().where<Intervention>().equalTo("id",jsonObject.getString("intervention")).findFirst()
        }

        startDate = if(jsonObject.getString("startDate") != "") dateFormatter.parse(jsonObject.getString("startDate")) else null
        completeDate = if(jsonObject.getString("completeDate") != "") dateFormatter.parse(jsonObject.getString("completeDate")) else null
        finished = jsonObject.getBoolean("finished")
        startingPoint = jsonObject.getString("startingPoint")
        finishingPoint = jsonObject.getString("finishingPoint")
        frequency = jsonObject.getString("frequency")
        if(jsonObject.has("comments")){
            for ( i in 0..(jsonObject.getJSONArray("comments").length() - 1)){
                val item = jsonObject.getJSONArray("comments").getString(i)
                comments.add(Realm.getDefaultInstance().where<Comments>().equalTo("id",item).findFirst())
            }
        }

    }
}
