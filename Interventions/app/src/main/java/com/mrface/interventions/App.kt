package com.mrface.interventions

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.facebook.drawee.backends.pipeline.Fresco
import com.mrface.interventions.Utils.Migration
import io.realm.Realm
import io.realm.RealmConfiguration

class App: MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
            .name("intervention.realm")
            .migration(Migration())
            .schemaVersion(1)
            .build()

        Realm.setDefaultConfiguration(realmConfig)
        Fresco.initialize(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}