package com.mrface.interventions.Activities

import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.WorkerThread
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.MenuItem
import com.mrface.interventions.Fragments.HomeFragment
import com.mrface.interventions.R
import com.mrface.interventions.models.InterventionSession
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState == null){

            if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),0)
            }

            supportActionBar?.title = ""
            supportActionBar?.setHomeButtonEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(false)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)

            supportFragmentManager
                .beginTransaction()
                .add(R.id.content_view,HomeFragment())
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){
            if(supportFragmentManager.backStackEntryCount > 0){
                supportFragmentManager.popBackStack()
            }
            return true
        }else{
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if(supportFragmentManager != null && supportFragmentManager.backStackEntryCount > 0){
            supportFragmentManager.popBackStack()
        }else{
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()

        GlobalScope.async {
            checkSessionsComplete()
        }
    }

    fun checkSessionsComplete(){
        val realm = Realm.getDefaultInstance()
        val sessions = realm.where<InterventionSession>().equalTo("finished",false).isNotNull("completeDate").findAll()
        for (session in sessions){
            if(session.completeDate!!.before(Date())){
                realm.executeTransaction {
                    session.finished = true
                }
            }
        }
    }
}
