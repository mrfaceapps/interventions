package com.mrface.interventions.Utils

import io.realm.DynamicRealm
import io.realm.RealmMigration

class Migration: RealmMigration {
    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        if(oldVersion == 0.toLong()){
            realm.schema.get("Student")!!
                .addField("pupilPremium",Boolean::class.java)
        }
    }

}