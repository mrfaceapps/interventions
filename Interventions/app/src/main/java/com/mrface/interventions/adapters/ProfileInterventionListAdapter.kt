package com.mrface.interventions.adapters

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.SessionItemListener
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.Utils.px
import com.mrface.interventions.models.Comments
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import io.realm.Sort
import kotlinx.android.synthetic.main.rv_double.view.*
import kotlinx.android.synthetic.main.rv_student_intervention.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ProfileInterventionListAdapter(val items: ArrayList<InterventionSession>, val context: Context, val listener: SessionItemListener) : RecyclerView.Adapter<InterventionHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): InterventionHolder {
        return InterventionHolder(LayoutInflater.from(context).inflate(R.layout.rv_student_intervention,p0,false)).listen{pos, type ->

            listener.onClick(items.get(pos))
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: InterventionHolder, p1: Int) {
        val item = items.get(p1)
        if(item.intervention != null){
            val border = GradientDrawable()
            border.cornerRadius = 20.px.toFloat()
            border.setColor(ContextCompat.getColor(context,R.color.LightGrey))
            border.setStroke(5.px,context.resources.getIntArray(R.array.colors)[item.intervention!!.colorIndex])
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
                p0.itemView.setBackgroundDrawable(border)
            }else{
                p0.itemView.background = border
            }
        }
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        if(item.intervention != null){
            p0.title.text = item.intervention!!.name
        }
        if(item.startDate != null){
            p0.startDateET.setText(dateFormat.format(item.startDate!!))
        }
        if(item.completeDate != null){
            p0.endDateET.setText(dateFormat.format(item.completeDate!!))

        }
        p0.startDateET.isEnabled = false
        p0.endDateET.isEnabled = false
        p0.startPositionET.isEnabled = false
        p0.endPositionET.isEnabled = false
        p0.frequencyET.isEnabled = false
        p0.startPositionET.setText(item.startingPoint)
        p0.endPositionET.setText(item.finishingPoint)
        p0.frequencyET.setText(item.frequency)
        var comments = ArrayList<Comments>(item.comments.sort("date",Sort.ASCENDING))
        p0.commentsRV.adapter = CommentAdapter(comments,context,null)
        p0.commentsRV.layoutManager = LinearLayoutManager(context)

    }

}
class InterventionHolder(view: View) : RecyclerView.ViewHolder(view) {
    val title = view.rv_student_intervention_title
    val startDateET = view.rv_student_intervention_start_date_ET
    val endDateET = view.rv_student_intervention_end_date_ET
    val startPositionET = view.rv_student_intervention_start_position_ET
    var endPositionET = view.rv_student_intervention_end_position_ET
    var frequencyET = view.rv_student_intervention_frequency_ET
    var commentsRV = view.rv_student_intervention_comment_RV
}