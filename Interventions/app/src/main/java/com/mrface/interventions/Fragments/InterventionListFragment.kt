package com.mrface.interventions.Fragments

import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.*
import android.text.InputType
import android.text.Layout
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.getbase.floatingactionbutton.AddFloatingActionButton
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.InterventionItemClickListener
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.VerticalSpaceItemDecoration
import com.mrface.interventions.adapters.InterventionListAdapter
import com.mrface.interventions.models.Intervention
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class InterventionListFragment : Fragment(), StudentItemClickListener, InterventionItemClickListener {
    val interventions: ArrayList<Intervention> = ArrayList()
    val sessions: ArrayList<InterventionSession> = ArrayList()
    var interventionListAdapter : InterventionListAdapter? = null
    var activeState: Boolean = true
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val main = activity as? MainActivity
        if(main != null){
            main.supportActionBar?.setHomeButtonEnabled(true)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            main.supportActionBar?.setDisplayShowHomeEnabled(true)
            main.supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(activity!!,R.color.colorPrimary)))
        }
        if(arguments != null && arguments!!.containsKey(Constants.ACTIVE_STUDENT_FLAG)){
            activeState = arguments!!.getBoolean(Constants.ACTIVE_STUDENT_FLAG,true)
        }
        val view : View = inflater.inflate(R.layout.fragment_list,null)
        main?.supportActionBar?.title = if(activeState) "Active Interventions" else "Inactive Interventions"
        if(!activeState){
            view.findViewById<AddFloatingActionButton>(R.id.fragment_add_btn).visibility = View.GONE
        }
        view.findViewById<AddFloatingActionButton>(R.id.fragment_add_btn).setOnClickListener { v ->
            val inputView = LayoutInflater.from(activity!!.applicationContext).inflate(R.layout.single_edit,null,false)

            var nameET = inputView.findViewById<EditText>(R.id.single_edit_et)
            nameET.hint = "Enter Intervention Name"
            nameET.setOnKeyListener { view, i, keyEvent ->
                if((keyEvent.action == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)){
                    true
                }
                false
            }
            AlertDialog.Builder(activity!!).setTitle("Create Intervention").setView(inputView).setCancelable(true).setPositiveButton("Add") {dialogView, i ->
                if(nameET.text.trim().isNotBlank() || nameET.text.trim().isNotEmpty()){

                    val realm = Realm.getDefaultInstance()
                    realm.executeTransaction {
                        var intervention = realm.createObject<Intervention>(UUID.randomUUID().toString())
                        intervention.name = nameET.text.toString()
                        val colorListSize = activity!!.resources.getIntArray(R.array.colors).size - 1
                        var alreadyColorList = ArrayList<Int>()
                        for (tmp in interventions){
                            alreadyColorList.add(tmp.colorIndex)
                        }
                        var rand = (0..colorListSize).random()
                        while (alreadyColorList.contains(rand)) {
                            rand = (0..colorListSize).random()
                        }
                        intervention.colorIndex = rand

                    }
                    obtainInterventionUpdates()
                    dialogView.dismiss()
                }else{
                    nameET.error = "Required"
                }

            }.create().show()
        }
        var interventionList : RecyclerView = view.findViewById(R.id.fragment_list_RV)
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            interventionList.layoutManager = StaggeredGridLayoutManager(2,1)
        }else if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            interventionList.layoutManager = StaggeredGridLayoutManager(3,1)
        }else{
            interventionList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        }
        interventionListAdapter = InterventionListAdapter(interventions,sessions,activity!!.applicationContext,this,this)
        interventionList.adapter = interventionListAdapter
        interventionList.addItemDecoration(VerticalSpaceItemDecoration())

        obtainInterventionUpdates()

        return view
    }

    override fun onClick(item: Student) {
        val bundle = Bundle()
        bundle.putString(Constants.STUDENT_ID,item.id)

        var fragment = StudentProfileFragment()
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()?.replace(R.id.content_view,fragment)?.addToBackStack(null)?.commit()
    }

    override fun onClick(item: Intervention) {
        val bundle = Bundle()
        bundle.putString(Constants.INTERVENTION_ID,item.id)
        bundle.putBoolean(Constants.ACTIVE_STUDENT_FLAG,activeState)
        var fragment = InterventionFragment()
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()?.replace(R.id.content_view,fragment)?.addToBackStack(null)?.commit()
    }
    fun obtainInterventionUpdates(){
        val realm = Realm.getDefaultInstance()
        val tmpSessions = realm.where<InterventionSession>().notEqualTo("finished",activeState).findAll().sort("startDate",
            Sort.DESCENDING)
        sessions.clear()
        sessions.addAll(tmpSessions)
        interventions.clear()
        val rawInterventions = realm.where<Intervention>().notEqualTo(if(activeState) "deletedActive" else "deletedInactive",true).findAll()
        interventions.addAll(rawInterventions)
        interventions.sortBy(this::nameSort)
        interventionListAdapter?.notifyDataSetChanged()

    }

    fun nameSort(i: Intervention) : String = i.name

}