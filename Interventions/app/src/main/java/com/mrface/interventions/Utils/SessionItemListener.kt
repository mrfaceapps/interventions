package com.mrface.interventions.Utils

import com.mrface.interventions.models.InterventionSession

interface SessionItemListener {
    fun onClick(session : InterventionSession)
}