package com.mrface.interventions.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.models.InterventionSession
import kotlinx.android.synthetic.main.rv_double.view.*

class StudentListInterventionsAdapter(val items: ArrayList<InterventionSession>, val context: Context,val parentView: View) : RecyclerView.Adapter<StudentInterventionSessionViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): StudentInterventionSessionViewHolder {
        return StudentInterventionSessionViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_double,p0,false)).listen { position, type ->
            parentView.performClick()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: StudentInterventionSessionViewHolder, p1: Int) {
        val item = items.get(p1)
        p0.double2.visibility = View.GONE
        Log.d("StudentListAdapter","name: " + item.id)
        if(item.intervention != null){
            p0.double1.text = item.intervention!!.name
        }

    }

}
class StudentInterventionSessionViewHolder (view: View) : RecyclerView.ViewHolder(view){
    var double1 = view.rv_double_1
    var double2 = view.rv_double_2
}