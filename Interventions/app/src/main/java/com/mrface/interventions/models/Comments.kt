package com.mrface.interventions.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

@RealmClass
open class Comments : RealmObject(){
    @PrimaryKey
    var id = ""
    var date : Date? = null
    var comment : String = ""

    var fieldName1: String = ""
    var fieldProgress1: String = ""
    var fieldName2: String = ""
    var fieldProgress2: String = ""
    var fieldName3: String = ""
    var fieldProgress3: String = ""
    var fieldName4: String = ""
    var fieldProgress4: String = ""

    var imageSrc: String = ""

    fun toJson():JSONObject {
        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
        var obj = JSONObject()

        obj.put("id",id);
        obj.put("date",if(date != null) dateFormatter.format(date) else "");
        obj.put("comment",comment);
        obj.put("fieldName1",fieldName1);
        obj.put("fieldProgress1",fieldProgress1);
        obj.put("fieldName2",fieldName2);
        obj.put("fieldProgress2",fieldProgress2);
        obj.put("fieldName3",fieldName3);
        obj.put("fieldProgress3",fieldProgress3);
        obj.put("fieldName4",fieldName4);
        obj.put("fieldProgress4",fieldProgress4);

        return obj
    }

    fun fromJson(jsonObject: JSONObject) {

        val dateFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

        date = if(jsonObject.getString("date") != "") dateFormatter.parse(jsonObject.getString("date")) else null
        comment = jsonObject.getString("comment")
        fieldName1 = jsonObject.getString("fieldName1")
        fieldProgress1 = jsonObject.getString("fieldProgress1")
        fieldName2 = jsonObject.getString("fieldName2")
        fieldProgress2 = jsonObject.getString("fieldProgress2")
        fieldName3 = jsonObject.getString("fieldName3")
        fieldProgress3 = jsonObject.getString("fieldProgress3")
        fieldName4 = jsonObject.getString("fieldName4")
        fieldProgress4 = jsonObject.getString("fieldProgress4")
    }
}