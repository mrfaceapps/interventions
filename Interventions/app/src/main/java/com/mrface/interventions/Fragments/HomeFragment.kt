package com.mrface.interventions.Fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.models.Comments
import com.mrface.interventions.models.Intervention
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import com.obsez.android.lib.filechooser.ChooserDialog
import com.squareup.picasso.Picasso
import dmax.dialog.SpotsDialog
import io.realm.Realm
import io.realm.internal.android.JsonUtils
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipFile
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val main = activity as? MainActivity
        if(main != null){
            main.supportActionBar?.title = ""
            main.supportActionBar?.setHomeButtonEnabled(false)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(false)
            main.supportActionBar?.setDisplayShowHomeEnabled(false)
            main.supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(activity!!,R.color.colorPrimary)))
        }
        val view: View = inflater.inflate(R.layout.fragment_home, null)
        setHasOptionsMenu(true)
        view.findViewById<Button>(R.id.fragment_home_intervention_btn).setOnClickListener { v ->
            fragmentManager?.beginTransaction()?.replace(R.id.content_view,InterventionListFragment())?.addToBackStack(null)?.commit()
        }

        view.findViewById<Button>(R.id.fragment_home_add_student_btn).setOnClickListener { v ->
            fragmentManager?.beginTransaction()?.replace(R.id.content_view,StudentProfileFragment())?.addToBackStack(null)?.commit()
        }

        view.findViewById<Button>(R.id.fragment_home_inactive_btn).setOnClickListener {
            val args = Bundle()
            args.putBoolean(Constants.ACTIVE_STUDENT_FLAG,false)
            val fragment = InterventionListFragment()
            fragment.arguments = args
            fragmentManager?.beginTransaction()?.replace(R.id.content_view,fragment)?.addToBackStack(null)?.commit()
        }

        view.findViewById<Button>(R.id.fragment_home_all_students_btn).setOnClickListener { v ->
            fragmentManager?.beginTransaction()?.replace(R.id.content_view,StudentsListFragment())?.addToBackStack(null)?.commit()
        }

        val parentDir = File(Environment.getExternalStorageDirectory(),"Interventions")
        if(!parentDir.exists()){
            parentDir.mkdirs()
        }

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == R.id.export){

            exportDbToZip()
            return true
        }else if(item?.itemId == R.id.upload){
            openImportFileBrowser()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun exportDbToZip(){
        val dialog = SpotsDialog.Builder().setContext(this.activity).build()
        dialog.show()
        val dateFormat = SimpleDateFormat("dd_MM_yyyy_HH_mm_ss")
        val realm = Realm.getDefaultInstance()
        val studentsArr = JSONArray()
        for (student in realm.where<Student>().findAll()){
            studentsArr.put(student.toJson())
        }
        val interventionsArr = JSONArray()
        for (intervention in realm.where<Intervention>().findAll()){
            interventionsArr.put(intervention.toJson())
        }
        val sessionsArr = JSONArray()
        for( session in realm.where<InterventionSession>().findAll()){
            sessionsArr.put(session.toJson())
        }
        val commentsArr = JSONArray()
        for( comment in realm.where<Comments>().findAll()){
            commentsArr.put(comment.toJson())
        }
        val exportObj = JSONObject()
        exportObj.put("Students",studentsArr)
        exportObj.put("Interventions",interventionsArr)
        exportObj.put("Sessions",sessionsArr)
        exportObj.put("Comments",commentsArr)

        val dir = File(Environment.getExternalStorageDirectory(),"Intervention Exports")
        if(!dir.exists()){
            dir.mkdirs()
        }
        val exportFile = File(dir,"export.json")
        exportFile.writeText(exportObj.toString())

        val imageDir = File(Environment.getExternalStorageDirectory(),"Interventions")

        if(!imageDir.exists()){
            imageDir.mkdirs()
        }
        Log.d("HomeFragment", imageDir.list().contentToString())
        val exportName = "interventions_export_" + dateFormat.format(Date()) + ".zip"
        val exportZip = File(dir,exportName);
        ZipOutputStream(BufferedOutputStream(FileOutputStream(exportZip))).use { out ->
            for (file in imageDir.list()) {
                FileInputStream(File(imageDir,file)).use { fi ->
                    BufferedInputStream(fi).use { origin ->
                        val entry = ZipEntry(file)
                        out.putNextEntry(entry)
                        origin.copyTo(out, 1024)
                    }
                }
            }
            FileInputStream(exportFile).use { fi ->
                BufferedInputStream(fi).use { origin ->
                    val entry = ZipEntry(exportFile.name)
                    out.putNextEntry(entry)
                    origin.copyTo(out, 1024)
                }
            }
        }
        exportFile.delete()
        dialog.dismiss()
        Toast.makeText(this.activity,"Export Complete",Toast.LENGTH_SHORT).show()
    }

    fun openImportFileBrowser(){
        ChooserDialog(activity)
            .withFilter(false,false,"zip")
            .withStartFile(Environment.getExternalStorageDirectory().toString())
            .withChosenListener { s, file ->
                importZipToDb(file)
            }
            .withOnCancelListener { dialog ->
                dialog.cancel()
            }
            .build()
            .show()
    }


    fun importZipToDb(selectedFile: File){
        val dialog = SpotsDialog.Builder().setContext(this.activity).build()
        dialog.show()
        if (!selectedFile.name.contains(".zip")){
            Toast.makeText(activity,"Not valid zip file",Toast.LENGTH_LONG).show()
            dialog.dismiss()
            return
        }
        val interventionDir = File(Environment.getExternalStorageDirectory(),"Interventions")
        val parentDir = File(Environment.getExternalStorageDirectory(),"Intervention Exports")
        if(!parentDir.exists()){
            parentDir.mkdirs()
        }
        val extractDir = File(parentDir,"extract")
        if(!extractDir.exists()){
            extractDir.mkdirs()
        }
        Log.d("HomeFragment","selectedFile: " + selectedFile.path)
        ZipFile(selectedFile).use { zip ->
            zip.entries().asSequence().forEach { entry ->
                zip.getInputStream(entry).use { input ->

                    File(extractDir,entry.name).outputStream().use { output ->
                        Log.d("HomeFragment","name: " + entry.name)
                        input.copyTo(output)
                    }
                }
            }
        }
        val exportFile = File(extractDir,"export.json")

        if(!exportFile.exists()){
            Log.d("HomeFragment","Doesn't contain export.json")
            Toast.makeText(activity,"Doesn't contain export.json",Toast.LENGTH_LONG).show()
            dialog.dismiss()
            extractDir.delete()
            return
        }


        try {
             JSONObject(exportFile.readText())
        }catch (e: Exception){
            e.printStackTrace()
            Log.d("HomeFragment","Not valid JSON")
            Toast.makeText(activity,"Not valid JSON",Toast.LENGTH_LONG).show()
            dialog.dismiss()
            extractDir.delete()
            return
        }

        val jsonObj = JSONObject(exportFile.readText())
        if (!(jsonObj.has("Students") && jsonObj.has("Interventions") && jsonObj.has("Sessions") && jsonObj.has("Comments"))){
            Log.d("HomeFragment","Not valid Interventions export")
            Toast.makeText(activity,"Not valid Interventions export",Toast.LENGTH_LONG).show()
            dialog.dismiss()
            extractDir.delete()
            return
        }

        AlertDialog.Builder(activity).setTitle("IMPORTANT").setMessage("Importing from this backup will remove all existing data stored in this app. Are you sure you want to continue?")
            .setPositiveButton("Yes") { d, which ->
                d.dismiss()
                Realm.getDefaultInstance().executeTransaction { realm ->

                    //TODO: Add confirmation

                    realm.deleteAll()
                    if(interventionDir.list() != null && interventionDir.list().isNotEmpty()){
                        for (file in interventionDir.list()){
                            File(interventionDir,file).delete()
                        }
                    }

                    for (i in 0..(jsonObj.getJSONArray("Students").length() - 1)){
                        val item = jsonObj.getJSONArray("Students").getJSONObject(i)
                        val student = realm.createObject<Student>(item.getString("id"))
                        student.fromJson(item)
                    }

                    for (i in 0..(jsonObj.getJSONArray("Interventions").length() - 1)){
                        val item = jsonObj.getJSONArray("Interventions").getJSONObject(i)
                        val intervention = realm.createObject<Intervention>(item.getString("id"))
                        intervention.fromJson(item)
                    }

                    for (i in 0..(jsonObj.getJSONArray("Comments").length() - 1)){
                        val item = jsonObj.getJSONArray("Comments").getJSONObject(i)
                        val comment = realm.createObject<Comments>(item.getString("id"))
                        comment.fromJson(item)
                    }

                    for (i in 0..(jsonObj.getJSONArray("Sessions").length() - 1)){
                        val item = jsonObj.getJSONArray("Sessions").getJSONObject(i)
                        val session = realm.createObject<InterventionSession>(item.getString("id"))
                        session.fromJson(item)
                    }
                }
                exportFile.delete()

                for(file in extractDir.list()){
                    File(extractDir,file).copyTo(File(interventionDir,file),true)
                }
                extractDir.deleteRecursively()
                dialog.dismiss()
                Toast.makeText(this.activity,"Import Complete",Toast.LENGTH_SHORT).show()
            }
            .setNegativeButton("No") { dialog, which ->
                extractDir.deleteRecursively()
                dialog.dismiss()
            }
            .create().show()

    }
}