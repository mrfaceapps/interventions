package com.mrface.interventions.Fragments

import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.getbase.floatingactionbutton.AddFloatingActionButton
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.px
import com.mrface.interventions.adapters.StudentListAdapter
import com.mrface.interventions.models.Student
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.where
import java.util.*

class StudentsListFragment : Fragment(), StudentItemClickListener {

    var adapter: StudentListAdapter? = null
    var students: ArrayList<Student> = ArrayList()
    var activeStudents: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val main = activity as? MainActivity
        if(main != null){
            main.supportActionBar?.setHomeButtonEnabled(true)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            main.supportActionBar?.setDisplayShowHomeEnabled(true)
            main.supportActionBar?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(activity!!,R.color.colorPrimary)))
        }
        if(arguments != null && arguments!!.containsKey(Constants.ACTIVE_STUDENT_FLAG)){
            activeStudents = arguments!!.getBoolean(Constants.ACTIVE_STUDENT_FLAG,true)
        }
        val view = inflater.inflate(R.layout.fragment_list, null)
        main?.supportActionBar?.title = "All Students"
        val studentList = view.findViewById<RecyclerView>(R.id.fragment_list_RV)
        studentList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        adapter = StudentListAdapter(students,activity!!.applicationContext,this)
        studentList.adapter = adapter


        val border = GradientDrawable()
        border.cornerRadius = 20.px.toFloat()
        border.setColor(ContextCompat.getColor(activity!!.applicationContext,R.color.LightGrey))
        border.setStroke(5.px, ContextCompat.getColor(activity!!.applicationContext,R.color.colorPrimary))
        view.findViewById<View>(R.id.fragment_list_background_view).background = border
        view.findViewById<AddFloatingActionButton>(R.id.fragment_add_btn).setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.content_view,StudentProfileFragment())?.addToBackStack(null)?.commit()
        }
        obtainStudents()
        return view
    }

    override fun onResume() {
        super.onResume()
        obtainStudents()
    }

    fun obtainStudents(){
        var realm = Realm.getDefaultInstance()
        var tmpStudents = realm.where<Student>()
            .findAll().sort("lastname",Sort.ASCENDING)
        if(tmpStudents != null){
            students.clear()
            students.addAll(tmpStudents)
            Log.d("StudentListFragment", students.toString())
            adapter?.notifyDataSetChanged()
        }
    }

    override fun onClick(item: Student) {
        val bundle = Bundle()
        bundle.putString(Constants.STUDENT_ID,item.id)

        var fragment = StudentProfileFragment()
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()?.replace(R.id.content_view,fragment)?.addToBackStack(null)?.commit()
    }
}