package com.mrface.interventions.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.InterventionItemClickListener
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.models.InterventionSession
import kotlinx.android.synthetic.main.rv_double.view.*

class InterventionStudentSessionAdapter(val items: ArrayList<InterventionSession>, val context: Context,val parentView: View,val listener:StudentItemClickListener) : RecyclerView.Adapter<SessionViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SessionViewHolder {
        return SessionViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_double,p0,false)).listen { position, type ->
            val item = items.get(position)
            if(item.student != null){
                listener.onClick(item.student!!)
            }else{
                parentView.performClick()
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: SessionViewHolder, p1: Int) {
        val item = items.get(p1)
        p0.double2.visibility = View.GONE
        if(item.student != null){
            p0.double1.text = item.student!!.firstname + " " + item.student!!.lastname
        }
    }

}
class SessionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val double1 = view.rv_double_1
    val double2 = view.rv_double_2
}