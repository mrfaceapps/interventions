package com.mrface.interventions.Fragments

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import android.widget.EditText
import android.widget.TextView
import com.getbase.floatingactionbutton.FloatingActionButton
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.CommentOnClickListener
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.px
import com.mrface.interventions.adapters.CommentAdapter
import com.mrface.interventions.models.Comments
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import com.stfalcon.frescoimageviewer.ImageViewer
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import org.w3c.dom.Comment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ProfileInterventionSessionFragment : Fragment(), CommentOnClickListener {

    var adapter: CommentAdapter? = null
    var tmpComments: ArrayList<Comments> = ArrayList()
    var sessionId = ""
    var session: InterventionSession? = null
    var startDateET: EditText? = null
    var endDateET: EditText? = null
    var startPositionET: EditText? = null
    var endPositionET: EditText? = null
    var frequencyET: EditText? = null
    var addBtn: FloatingActionsMenu? = null
    var commentsBtn: FloatingActionButton? = null
    var reportBtn: FloatingActionButton? = null
    var cameraBtn: FloatingActionButton? = null
    var randUUID: String = ""
    var commentUri: Uri? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val main = activity as? MainActivity
        if(main != null){
            main.supportActionBar?.setHomeButtonEnabled(true)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            main.supportActionBar?.setDisplayShowHomeEnabled(true)
        }
        setHasOptionsMenu(true)

        val view: View = inflater.inflate(R.layout.fragment_student_intervention,null)

        startDateET = view.findViewById(R.id.rv_student_intervention_start_date_ET)
        endDateET = view.findViewById(R.id.rv_student_intervention_end_date_ET)
        startPositionET = view.findViewById(R.id.rv_student_intervention_start_position_ET)
        endPositionET = view.findViewById(R.id.rv_student_intervention_end_position_ET)
        frequencyET = view.findViewById(R.id.rv_student_intervention_frequency_ET)
        addBtn = view.findViewById(R.id.fragment_add_btn)

        cameraBtn = FloatingActionButton(activity!!)
        cameraBtn?.title = "Camera"
        cameraBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.camera_icon)!!)
        cameraBtn?.setOnClickListener {
            if(randUUID == ""){
                randUUID = UUID.randomUUID().toString()
            }
            val path = Environment.getExternalStorageDirectory().toString()

            val folder = File(path,"Interventions")

            if(!folder.exists()){
                folder.mkdirs()
            }
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                commentUri = Uri.parse("content://${folder.absolutePath}/${randUUID}_image.png")
            }else{
                commentUri = Uri.parse("file://${folder.absolutePath}/${randUUID}_image.png")
            }

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT,commentUri)
            intent.resolveActivity(activity!!.packageManager).also {
                startActivityForResult(intent, Constants.COMMENT_IMAGE_REQUEST)
            }
        }
        commentsBtn = FloatingActionButton(activity!!)
        commentsBtn?.title = "Comment"
        commentsBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.outline_chat_bubble)!!)
        commentsBtn?.setOnClickListener {


            var builder = AlertDialog.Builder(activity!!)
            builder.setTitle("Create Comment")

            val inputView = LayoutInflater.from(activity!!).inflate(R.layout.comment_input_layout,null)
            val commentET = inputView.findViewById<EditText>(R.id.comment_input_comment)

            builder.setView(inputView)
            builder.setCancelable(true)
            builder.setPositiveButton("Add") { dialogInterface, i ->
                if(commentET.text.trim().isEmpty()){
                    commentET.error = "Required"
                }else{
                    val realm = Realm.getDefaultInstance()
                    realm.executeTransaction {
                        val comment = realm.createObject<Comments>(UUID.randomUUID().toString())
                        comment.date = Date()
                        comment.comment = commentET.text.toString()
                        session?.comments?.add(comment)
                    }
                    obtainComments()
                    dialogInterface.dismiss()
                }
            }
            builder.create().show()
        }

        reportBtn = com.getbase.floatingactionbutton.FloatingActionButton(activity!!)
        reportBtn?.title = "Progress Report"
        reportBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.outline_assessment_white_48dp)!!)
        reportBtn?.setOnClickListener {

            var builder = AlertDialog.Builder(activity!!)
            builder.setTitle("Create Progress Report")

            val inputView = LayoutInflater.from(activity!!).inflate(R.layout.rv_comment_progress,null)
            val field1ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_1_name_ET)
            val progress1ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_1_ET)
            val field2ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_2_name_ET)
            val progress2ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_2_ET)
            val field3ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_3_name_ET)
            val progress3ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_3_ET)
            val field4ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_4_name_ET)
            val progress4ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_4_ET)

            builder.setView(inputView)
            builder.setCancelable(true)
            builder.setPositiveButton("Add") { dialogInterface, i ->

                Realm.getDefaultInstance().executeTransaction { realm ->
                    val comment = realm.createObject<Comments>(UUID.randomUUID().toString())
                    comment.date = Date()
                    if(field1ET.text.trim().toString().isNotEmpty() && field1ET.text.trim().toString().isNotBlank() ){
                        comment.fieldName1 = field1ET.text.toString()
                    }
                    if(progress1ET.text.trim().toString().isNotEmpty() && progress1ET.text.trim().toString().isNotBlank() ){
                        comment.fieldProgress1 = progress1ET.text.toString()
                    }

                    if(field2ET.text.trim() != comment.fieldName2){
                        comment.fieldName2 = field2ET.text.toString()
                    }
                    if(progress2ET.text.trim() != comment.fieldProgress2){
                        comment.fieldProgress2 = progress2ET.text.toString()
                    }

                    if(field3ET.text.trim() != comment.fieldName3){
                        comment.fieldName3 = field3ET.text.toString()
                    }
                    if(progress3ET.text.trim() != comment.fieldProgress3){
                        comment.fieldProgress3 = progress3ET.text.toString()
                    }

                    if(field4ET.text.trim() != comment.fieldName4){
                        comment.fieldName4 = field4ET.text.toString()
                    }
                    if(progress4ET.text.trim() != comment.fieldProgress4){
                        comment.fieldProgress4 = progress4ET.text.toString()
                    }
                    session?.comments?.add(comment)
                }
                obtainComments()
                dialogInterface.dismiss()
            }
            builder.create().show()
        }
        addBtn?.addButton(cameraBtn!!)
        addBtn?.addButton(commentsBtn!!)
        addBtn?.addButton(reportBtn!!)
        val keyListener: View.OnKeyListener = View.OnKeyListener { view, i, keyEvent ->
            if((keyEvent.action == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)){
                true
            }
            false
        }
        startPositionET?.setOnKeyListener(keyListener)
        endPositionET?.setOnKeyListener(keyListener)
        frequencyET?.setOnKeyListener(keyListener)


        var clickedView: View? = null
        var calendar = Calendar.getInstance()
        val dateListener = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            if(clickedView != null) {
                val dateFormat = SimpleDateFormat("dd/MM/yyyy")
                calendar.set(year,month,day)
                if(clickedView == startDateET){
                    startDateET?.setText(dateFormat.format(calendar.time))
                }else if(clickedView == endDateET){
                    endDateET?.setText(dateFormat.format(calendar.time))
                }
            }
        }

        addBtn?.visibility = View.VISIBLE

        startDateET?.setOnClickListener{v ->
            clickedView = startDateET
            if(session?.startDate != null){
                calendar.time = session!!.startDate
            }
            DatePickerDialog(activity,dateListener, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        endDateET?.setOnClickListener {v ->
            clickedView = endDateET
            if(session?.completeDate != null){
                calendar.time = session!!.completeDate
            }
            DatePickerDialog(activity,dateListener, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        if (arguments != null && arguments!!.containsKey(Constants.SESSION_ID)){
            sessionId = arguments!!.getString(Constants.SESSION_ID)
            val realm = Realm.getDefaultInstance()
            session = realm.where<InterventionSession>()
                .equalTo("id",sessionId)
                .findFirst()
            if(session != null){
                if(session!!.intervention != null){
                    view.findViewById<TextView>(R.id.rv_student_intervention_title).setText(session!!.intervention!!.name)
                    if(main != null){
                        main?.supportActionBar?.setBackgroundDrawable(ColorDrawable(resources.getIntArray(R.array.colors)[session!!.intervention!!.colorIndex]))
                    }
                }
                val dateFormat = SimpleDateFormat("dd/MM/yyyy")
                if(session!!.startDate != null){
                    startDateET?.setText(dateFormat.format(
                        session!!.startDate!!))
                }

                if(session!!.completeDate != null){
                    endDateET?.setText(dateFormat.format(session!!.completeDate!!))
                }

                startPositionET?.setText(session!!.startingPoint)
                endPositionET?.setText(session!!.finishingPoint)
                frequencyET?.setText(session!!.frequency)

                val border = GradientDrawable()
                border.cornerRadius = 20.px.toFloat()
                border.setColor(ContextCompat.getColor(activity!!.applicationContext,R.color.LightGrey))
                border.setStroke(5.px,activity!!.applicationContext.resources.getIntArray(R.array.colors)[session!!.intervention!!.colorIndex])
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
                    view.setBackgroundDrawable(border)
                }else{
                    view.background = border
                }

            }
        }

        val commentsRV = view.findViewById<RecyclerView>(R.id.rv_student_intervention_comment_RV)
        commentsRV.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        adapter = CommentAdapter(tmpComments,activity!!.applicationContext, this)
        commentsRV.adapter = adapter

        val simpleItemTouchCallback = object : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT){
            override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
                return true
            }

            override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
                val pos = p0.adapterPosition
                if(p1 == ItemTouchHelper.LEFT){
                    Realm.getDefaultInstance().executeTransaction {
                        tmpComments[pos].deleteFromRealm()
                        tmpComments.removeAt(pos)
                    }
                    adapter!!.notifyDataSetChanged()
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(commentsRV)
        obtainComments()

        return view
    }

    override fun onPause() {
        super.onPause()
        if(session != null){
            Realm.getDefaultInstance().executeTransaction {
                val dateFormat = SimpleDateFormat("dd/MM/yyyy")
                if(startDateET!!.text.toString().trim().isNotEmpty() && session!!.startDate != dateFormat.parse(startDateET!!.text.toString())){
                    session!!.startDate = dateFormat.parse(startDateET!!.text.toString())
                }

                if(endDateET!!.text.toString().trim().isNotEmpty() && session!!.completeDate != dateFormat.parse(endDateET!!.text.toString())){
                    session!!.completeDate = dateFormat.parse(endDateET!!.text.toString())
                    session?.finished = session?.completeDate != null && session!!.completeDate!!.before(Date())
                }

                if(startPositionET!!.text.toString().trim().isNotEmpty() && session!!.startingPoint != startPositionET!!.text.toString()){
                    session!!.startingPoint = startPositionET!!.text.toString()
                }

                if(endPositionET!!.text.toString().trim().isNotEmpty() && session!!.finishingPoint != endPositionET!!.text.toString()){
                    session!!.finishingPoint = endPositionET!!.text.toString()
                }

                if(frequencyET!!.text.toString().trim().isNotEmpty() && session!!.frequency != frequencyET!!.text.toString()){
                    session!!.frequency = frequencyET!!.text.toString()
                }
            }
        }
    }

    override fun onClick(comment: Comments, type: String) {
        val realm = Realm.getDefaultInstance()
        val tmpComment = realm.where<Comments>().equalTo("id",comment.id).findFirst()
        if(tmpComment != null){
            if(type == "comment-date"){
                var calendar = Calendar.getInstance()
                calendar.time = tmpComment.date
                DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                    calendar.set(year,month,day)
                    realm.executeTransaction {
                        tmpComment.date = calendar.time
                    }
                    obtainComments()
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show()
            }else if(type == "comment-comment"){

                var builder = AlertDialog.Builder(activity!!)
                builder.setTitle("Edit Comment")

                val inputView = LayoutInflater.from(activity!!).inflate(R.layout.comment_input_layout,null)
                val commentET = inputView.findViewById<EditText>(R.id.comment_input_comment)
                commentET.setOnKeyListener { view, i, keyEvent ->
                    if((keyEvent.action == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)){
                        true
                    }
                    false
                }
                commentET.setText(tmpComment.comment)
                builder.setView(inputView)
                builder.setCancelable(true)
                builder.setPositiveButton("Edit") { dialogInterface, i ->
                    if(commentET.text.trim().isEmpty()){
                        commentET.error = "Required"
                    }else{
                        realm.executeTransaction {
                            tmpComment.comment = commentET.text.toString()
                        }
                        obtainComments()
                        dialogInterface.dismiss()
                    }
                }
                builder.create().show()
            }else if(type == "progress-date"){
                var calendar = Calendar.getInstance()
                calendar.time = tmpComment.date
                DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                    calendar.set(year,month,day)
                    realm.executeTransaction {
                        tmpComment.date = calendar.time
                    }
                    obtainComments()
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show()
            }else if(type == "progress"){

                var builder = AlertDialog.Builder(activity!!)
                builder.setTitle("Edit Progress Report")

                val inputView = LayoutInflater.from(activity!!).inflate(R.layout.rv_comment_progress,null)
                val field1ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_1_name_ET)
                val progress1ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_1_ET)
                val field2ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_2_name_ET)
                val progress2ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_2_ET)
                val field3ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_3_name_ET)
                val progress3ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_3_ET)
                val field4ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_4_name_ET)
                val progress4ET = inputView.findViewById<EditText>(R.id.rv_comment_progress_field_4_ET)

                val tmpComment = realm.where<Comments>().equalTo("id",comment.id).findFirst()

                if(tmpComment != null){
                    field1ET.setText(tmpComment.fieldName1)
                    field2ET.setText(tmpComment.fieldName2)
                    field3ET.setText(tmpComment.fieldName3)
                    field4ET.setText(tmpComment.fieldName4)

                    progress1ET.setText(tmpComment.fieldProgress1)
                    progress2ET.setText(tmpComment.fieldProgress2)
                    progress3ET.setText(tmpComment.fieldProgress3)
                    progress4ET.setText(tmpComment.fieldProgress4)
                }
                builder.setView(inputView)
                builder.setCancelable(true)
                builder.setPositiveButton("Edit") { dialogInterface, i ->

                    Realm.getDefaultInstance().executeTransaction { realm ->
                        if(tmpComment != null){
                            if(field1ET.text.trim().toString().isNotEmpty() && field1ET.text.trim().toString().isNotBlank() ){
                                tmpComment.fieldName1 = field1ET.text.toString()
                            }
                            if(progress1ET.text.trim().toString().isNotEmpty() && progress1ET.text.trim().toString().isNotBlank() ){
                                tmpComment.fieldProgress1 = progress1ET.text.toString()
                            }

                            if(field2ET.text.trim() != tmpComment.fieldName2){
                                tmpComment.fieldName2 = field2ET.text.toString()
                            }
                            if(progress2ET.text.trim() != tmpComment.fieldProgress2){
                                tmpComment.fieldProgress2 = progress2ET.text.toString()
                            }

                            if(field3ET.text.trim() != tmpComment.fieldName3){
                                tmpComment.fieldName3 = field3ET.text.toString()
                            }
                            if(progress3ET.text.trim() != tmpComment.fieldProgress3){
                                tmpComment.fieldProgress3 = progress3ET.text.toString()
                            }

                            if(field4ET.text.trim() != tmpComment.fieldName4){
                                tmpComment.fieldName4 = field4ET.text.toString()
                            }
                            if(progress4ET.text.trim() != tmpComment.fieldProgress4){
                                tmpComment.fieldProgress4 = progress4ET.text.toString()
                            }
                        }
                    }
                    obtainComments()
                    dialogInterface.dismiss()
                }
                builder.create().show()
            }else if(type == "image-date"){
                var calendar = Calendar.getInstance()
                calendar.time = tmpComment.date
                DatePickerDialog(activity!!,DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                    calendar.set(year,month,day)
                    realm.executeTransaction {
                        tmpComment.date = calendar.time
                    }
                    obtainComments()
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show()
            }else if(type == "image"){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    ImageViewer.Builder(activity!!,arrayOf("content://" + tmpComment.imageSrc)).setStartPosition(0).show()
                }else{
                    ImageViewer.Builder(activity!!,arrayOf("file://" + tmpComment.imageSrc)).setStartPosition(0).show()
                }
            }
        }
    }

    fun obtainComments(){
        if(session != null){
            tmpComments.clear()
            tmpComments.addAll(ArrayList(session!!.comments.sort("date",Sort.ASCENDING)))

            adapter?.notifyDataSetChanged()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.save_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == R.id.save){
            if(session != null){
                Realm.getDefaultInstance().executeTransaction {
                    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
                    if(startDateET != null && startDateET!!.text.toString().trim().isNotEmpty() && startDateET!!.text.toString().trim().isNotBlank()){
                        session?.startDate = dateFormat.parse(startDateET!!.text.toString())
                    }
                    if(endDateET != null && endDateET!!.text.toString().trim().isNotEmpty() && endDateET!!.text.toString().trim().isNotBlank()){
                        session?.completeDate = dateFormat.parse(endDateET!!.text.toString())
                        session?.finished = session?.completeDate != null && session!!.completeDate!!.before(Date())
                    }
                    if(startPositionET != null  && startPositionET!!.text.toString().trim().isNotEmpty() && startPositionET!!.text.toString().trim().isNotBlank()){
                        session?.startingPoint = startPositionET!!.text.toString()
                    }
                    if(endPositionET != null  && endPositionET!!.text.toString().trim().isNotEmpty() && endPositionET!!.text.toString().trim().isNotBlank()){
                        session?.finishingPoint = endPositionET!!.text.toString()
                    }
                    if(frequencyET != null && frequencyET!!.text.toString().trim().isNotEmpty() && frequencyET!!.text.toString().trim().isNotBlank()){
                        session?.frequency = frequencyET!!.text.toString()
                    }
                    for(comment in tmpComments){
                        if(!session!!.comments.contains(comment)){
                            session?.comments?.add(comment)
                        }
                    }
                }
            }
            activity?.supportFragmentManager?.popBackStack()
            return true
        } else if(item?.itemId == R.id.delete) {


            AlertDialog.Builder(ContextThemeWrapper(activity!!,R.style.myDialog))
                .setTitle("Delete Confirmation")
                .setMessage("Are you sure you want to delete student from this intervention?")
                .setPositiveButton("Yes") { dialogInterface, i ->
                    if(session != null){
                        Realm.getDefaultInstance().executeTransaction { realm ->
                            session!!.deleteFromRealm()
                        }
                    }
                    dialogInterface.dismiss()
                    activity?.supportFragmentManager?.popBackStack()
                }
                .setNegativeButton("No") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
            return true
        }else {
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == Constants.COMMENT_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && commentUri != null){

            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                val comment = realm.createObject<Comments>(UUID.randomUUID().toString())
                comment.date = Date()
                comment.imageSrc = commentUri!!.path!!
                session?.comments?.add(comment)
            }
            obtainComments()
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}