package com.mrface.interventions.Utils

import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View

class VerticalSpaceItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.bottom = 5.px
        outRect.left = 5.px
        outRect.right = 5.px
        outRect.top = 5.px
    }
}