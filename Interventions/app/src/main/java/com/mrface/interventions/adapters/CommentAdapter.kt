package com.mrface.interventions.adapters

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mrface.interventions.R
import com.mrface.interventions.Utils.CommentOnClickListener
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.models.Comments
import com.mrface.interventions.models.InterventionSession
import com.squareup.picasso.Picasso
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.rv_comment_image.view.*
import kotlinx.android.synthetic.main.rv_comment_progress.view.*
import kotlinx.android.synthetic.main.rv_double.view.*
import kotlinx.android.synthetic.main.rv_student_intervention.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CommentAdapter(val items: ArrayList<Comments>, val context: Context, val listener: CommentOnClickListener?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return when {
            p1 == Constants.VIEW_TYPE_IMAGE -> ImageCommentViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.rv_comment_image,
                    p0,
                    false
                )
            )
            p1 == Constants.VIEW_TYPE_PROGRESS-> ProgressHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.rv_comment_progress,
                    p0,
                    false
                )
            )
            else -> CommentHolder(LayoutInflater.from(context).inflate(R.layout.rv_double, p0, false))
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        if(item.imageSrc != ""){
            return Constants.VIEW_TYPE_IMAGE
        }else if(item.fieldName1 != ""){
            return Constants.VIEW_TYPE_PROGRESS
        }else{
            return Constants.VIEW_TYPE_COMMENT
        }
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        var dateFormat = SimpleDateFormat("dd/MM/yyyy")
        val item = items[p1]
        if (item.imageSrc != "") {
            val viewHolder = p0 as ImageCommentViewHolder

            viewHolder.dateTV.text = dateFormat.format(item.date)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                Picasso.get().load("content://" + item.imageSrc).into(viewHolder.imageIV)
            }else{
                Picasso.get().load("file://" + item.imageSrc).into(viewHolder.imageIV)
            }

            if (listener != null) {
                viewHolder.dateTV.isClickable = true
                viewHolder.dateTV.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "image-date")
                }
                viewHolder.imageIV.isClickable = true
                viewHolder.imageIV.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "image")
                }
            }
        } else if (item.fieldName1.trim() != "") {
            val viewHolder = p0 as ProgressHolder

            viewHolder.dateTV.setText(dateFormat.format(item.date))
            viewHolder.fieldName1.setText(item.fieldName1)
            viewHolder.fieldName2.setText(item.fieldName2)
            viewHolder.fieldName3.setText(item.fieldName3)
            viewHolder.fieldName4.setText(item.fieldName4)

            viewHolder.fieldProgress1.setText(item.fieldProgress1)
            viewHolder.fieldProgress2.setText(item.fieldProgress2)
            viewHolder.fieldProgress3.setText(item.fieldProgress3)
            viewHolder.fieldProgress4.setText(item.fieldProgress4)

            if (item.fieldName2.trim().isBlank() || item.fieldName2.trim().isEmpty()) {
                viewHolder.fieldName2.visibility = View.GONE
            } else {
                p0.fieldName2.visibility = View.VISIBLE
            }
            if (item.fieldProgress2.trim().isBlank() || item.fieldProgress2.trim().isEmpty()) {
                viewHolder.fieldProgress2.visibility = View.GONE
            } else {
                viewHolder.fieldProgress2.visibility = View.VISIBLE
            }
            if (item.fieldName3.trim().isBlank() || item.fieldName3.trim().isEmpty()) {
                viewHolder.fieldName3.visibility = View.GONE
            } else {
                viewHolder.fieldName3.visibility = View.VISIBLE
            }
            if (item.fieldProgress3.trim().isBlank() || item.fieldProgress3.trim().isEmpty()) {
                viewHolder.fieldProgress3.visibility = View.GONE
            } else {
                viewHolder.fieldProgress3.visibility = View.VISIBLE
            }
            if (item.fieldName4.trim().isBlank() || item.fieldName4.trim().isEmpty()) {
                viewHolder.fieldName4.visibility = View.GONE
            } else {
                viewHolder.fieldName4.visibility = View.VISIBLE
            }
            if (item.fieldProgress4.trim().isBlank() || item.fieldProgress4.trim().isEmpty()) {
                viewHolder.fieldProgress4.visibility = View.GONE
            } else {
                viewHolder.fieldProgress4.visibility = View.VISIBLE
            }

            if (listener != null) {
                viewHolder.dateTV.isClickable = true
                viewHolder.dateTV.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress-date")
                }
                viewHolder.fieldName1.isClickable = true
                viewHolder.fieldName1.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldName2.isClickable = true
                viewHolder.fieldName2.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldName3.isClickable = true
                viewHolder.fieldName3.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldName4.isClickable = true
                viewHolder.fieldName4.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldProgress1.isClickable = true
                viewHolder.fieldProgress1.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldProgress2.isClickable = true
                viewHolder.fieldProgress2.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldProgress3.isClickable = true
                viewHolder.fieldProgress3.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
                viewHolder.fieldProgress4.isClickable = true
                viewHolder.fieldProgress4.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "progress")
                }
            }
            viewHolder.fieldName1.isFocusable = false
            viewHolder.fieldName2.isFocusable = false
            viewHolder.fieldName3.isFocusable = false
            viewHolder.fieldName4.isFocusable = false
            viewHolder.fieldProgress1.isFocusable = false
            viewHolder.fieldProgress2.isFocusable = false
            viewHolder.fieldProgress3.isFocusable = false
            viewHolder.fieldProgress4.isFocusable = false
        } else {
            val viewHolder = p0 as CommentHolder
            viewHolder.double1.typeface = Typeface.DEFAULT_BOLD
            if (items.get(p1).date != null) {
                viewHolder.double1.text = dateFormat.format(items.get(p1).date!!)
            }
            viewHolder.double2.text = items.get(p1).comment
            if (listener != null) {
                viewHolder.double1.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "comment-date")
                }
                viewHolder.double2.setOnClickListener {
                    val comment = items.get(p1)
                    listener.onClick(comment, "comment-comment")
                }
            }
        }
    }

}

class CommentHolder(view: View) : RecyclerView.ViewHolder(view) {
    var double1 = view.rv_double_1
    var double2 = view.rv_double_2
}

class ImageCommentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var dateTV = view.rv_comment_image_date_tv
    var imageIV = view.rv_comment_image_iv
}

class ProgressHolder(view: View) : RecyclerView.ViewHolder(view) {
    var dateTV = view.rv_comment_progress_date_TV
    var fieldName1 = view.rv_comment_progress_field_1_name_ET
    var fieldProgress1 = view.rv_comment_progress_field_1_ET
    var fieldName2 = view.rv_comment_progress_field_2_name_ET
    var fieldProgress2 = view.rv_comment_progress_field_2_ET
    var fieldName3 = view.rv_comment_progress_field_3_name_ET
    var fieldProgress3 = view.rv_comment_progress_field_3_ET
    var fieldName4 = view.rv_comment_progress_field_4_name_ET
    var fieldProgress4 = view.rv_comment_progress_field_4_ET
}