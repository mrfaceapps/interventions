package com.mrface.interventions.Utils

import android.app.ActivityManager
import android.content.Context
import android.util.TypedValue

class Constants {
    companion object {
        const val INTERVENTION_ID = "intervention_id"
        const val STUDENT_ID = "student_id"
        const val SESSION_ID = "session_id"
        const val LUCID_PHOTO_REQUEST = 1
        const val RAPID_PHOTO_REQUEST = 2
        const val COMMENT_IMAGE_REQUEST = 3
        const val ACTIVE_STUDENT_FLAG = "active_student_flag"
        const val VIEW_TYPE_IMAGE = 100
        const val VIEW_TYPE_PROGRESS = 101
        const val VIEW_TYPE_COMMENT = 102

    }
}