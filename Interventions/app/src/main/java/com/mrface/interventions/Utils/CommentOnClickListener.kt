package com.mrface.interventions.Utils

import com.mrface.interventions.models.Comments
import java.util.*

interface CommentOnClickListener {
    fun onClick(comment: Comments, type: String)
}