package com.mrface.interventions.Utils

import android.content.res.Resources
import android.support.v7.widget.RecyclerView

val Int.dp: Int
        get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
        get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(getAdapterPosition(), getItemViewType())
    }
    return this
}