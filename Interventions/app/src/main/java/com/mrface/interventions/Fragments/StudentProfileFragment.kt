package com.mrface.interventions.Fragments

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DialogFragment
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.*
import com.getbase.floatingactionbutton.AddFloatingActionButton
import com.getbase.floatingactionbutton.FloatingActionsMenu
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.SessionItemListener
import com.mrface.interventions.Utils.VerticalSpaceItemDecoration
import com.mrface.interventions.Utils.px
import com.mrface.interventions.adapters.ProfileInterventionListAdapter
import com.mrface.interventions.models.Comments
import com.mrface.interventions.models.Intervention
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import com.stfalcon.frescoimageviewer.ImageViewer
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class StudentProfileFragment : Fragment(), SessionItemListener {
    var studentId: String = ""
    var sessions: ArrayList<InterventionSession> = ArrayList()
    var adapter: ProfileInterventionListAdapter? = null
    var dobET: EditText? = null
    var nameET: EditText? = null
    var tagsET: EditText? = null
    var lastnameET: EditText? = null
    var quickDateET: EditText? = null
    var quickPumaET: EditText? = null
    var quickPiraET: EditText? = null
    var quickReadingAgeET: EditText? = null
    var quickWritingET: EditText? = null
    var pupilPremiumCheck: CheckBox? = null
    var student: Student? = null
    var lucidBtn: Button? = null
    var rapidBtn: Button? = null
    var randUUID: String = ""
    var lucidUri: Uri? = null
    var rapidUri: Uri? = null
    var addInterventionBtn: com.getbase.floatingactionbutton.FloatingActionButton? = null
    var addRapidBtn: com.getbase.floatingactionbutton.FloatingActionButton? = null
    var addRecallBtn: com.getbase.floatingactionbutton.FloatingActionButton? = null
    var menuBtn: FloatingActionsMenu? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val main = activity as? MainActivity
        if (main != null) {
            main.supportActionBar?.setHomeButtonEnabled(true)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            main.supportActionBar?.setDisplayShowHomeEnabled(true)
            main.supportActionBar?.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.colorPrimary
                    )
                )
            )
        }
        setHasOptionsMenu(true)
        val view: View = inflater.inflate(R.layout.fragment_student_profile, null)
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")
        dobET = view.findViewById(R.id.fragment_student_year_group_ET)
        nameET = view.findViewById(R.id.fragment_student_name_ET)
        tagsET = view.findViewById(R.id.fragment_student_tags_ET)
        lucidBtn = view.findViewById(R.id.fragment_student_lucid_btn)
        rapidBtn = view.findViewById(R.id.fragment_student_rapid_btn)
        lastnameET = view.findViewById(R.id.fragment_student_last_name_ET)
        menuBtn = view.findViewById(R.id.fragment_add_btn)
        quickDateET = view.findViewById(R.id.fragment_student_quick_date_ET)
        quickPumaET = view.findViewById(R.id.fragment_student_quick_puma_ET)
        quickPiraET = view.findViewById(R.id.fragment_student_quick_pira_ET)
        quickReadingAgeET = view.findViewById(R.id.fragment_student_quick_reading_age_ET)
        quickWritingET = view.findViewById(R.id.fragment_student_quick_writing_ET)
        pupilPremiumCheck = view.findViewById(R.id.fragment_student_pp_check)

        val border = GradientDrawable()
        border.cornerRadius = 20.px.toFloat()
        border.setColor(ContextCompat.getColor(activity!!.applicationContext,R.color.LightGrey))
        border.setStroke(5.px,ContextCompat.getColor(activity!!.applicationContext,R.color.colorPrimary))

        view.findViewById<ConstraintLayout>(R.id.fragment_student_quick_layout).background = border

        var calendar = Calendar.getInstance()
        var quickCalendar = Calendar.getInstance()
        val dateListener = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            val dateFormat = SimpleDateFormat("dd/MM/yyyy")
            calendar.set(year, month, day)
            dobET?.setText(dateFormat.format(calendar.time))
        }
        val quickDateListener = DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
            val dateFormat = SimpleDateFormat("dd/MM/yyyy")
            quickCalendar.set(year, month, day)
            quickDateET?.setText(dateFormat.format(quickCalendar.time))
        }
        val keyListener: View.OnKeyListener = View.OnKeyListener { view, i, keyEvent ->
            if ((keyEvent.action == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                true
            }
            false
        }
        nameET?.setOnKeyListener(keyListener)
        lastnameET?.setOnKeyListener(keyListener)
        tagsET?.setOnKeyListener(keyListener)
//        quickDateET?.setOnClickListener {
//            DatePickerDialog(
//                activity, quickDateListener, quickCalendar.get(Calendar.YEAR), quickCalendar.get(Calendar.MONTH), quickCalendar.get(
//                    Calendar.DAY_OF_MONTH
//                )
//            ).show()
//        }
        quickWritingET?.setOnKeyListener(keyListener)
        quickReadingAgeET?.setOnKeyListener(keyListener)
        quickPiraET?.setOnKeyListener(keyListener)
        quickPumaET?.setOnKeyListener(keyListener)
        dobET?.setOnClickListener { v ->
            DatePickerDialog(
                activity, dateListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(
                    Calendar.DAY_OF_MONTH
                )
            ).show()
        }
        if (arguments != null && arguments!!.containsKey(Constants.STUDENT_ID)) {
            studentId = arguments!!.getString(Constants.STUDENT_ID)
            val realm = Realm.getDefaultInstance()
            student = realm.where<Student>()
                .equalTo("id", studentId)
                .findFirst()
            if (student != null) {
                nameET?.setText(student!!.firstname)
                lastnameET?.setText(student!!.lastname)
                if (student?.dob != null) {
                    calendar.time = student!!.dob!!
                    dobET?.setText(dateFormat.format(student!!.dob!!))
                }
                quickDateET?.setText(student!!.quickDate)
                quickPumaET?.setText(student!!.quickPuma)
                quickPiraET?.setText(student!!.quickPira)
                quickReadingAgeET?.setText(student!!.quickReadingAge)
                quickWritingET?.setText(student!!.quickWriting)
                pupilPremiumCheck?.isChecked = student!!.pupilPremium
                sessions.clear()
                val realm = Realm.getDefaultInstance()
                sessions.addAll(
                    realm.where<InterventionSession>().equalTo("student.id",student!!.id).findAll().sort("startDate",Sort.DESCENDING).sort("intervention.name").sort("finished",Sort.ASCENDING)
                )
            }
        }

        lucidBtn?.setOnClickListener {
            if(student == null){
                launchCamera(false)
            }else if(student!!.lucid.trim().isNotBlank() && student!!.lucid.trim().isNotEmpty()){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    ImageViewer.Builder(activity!!,arrayOf("content://" + student!!.lucid)).setStartPosition(0).show()
                }else{
                    ImageViewer.Builder(activity!!,arrayOf("file://" + student!!.lucid)).setStartPosition(0).show()
                }
            }
        }
        rapidBtn?.setOnClickListener {
            if(student == null){
                launchCamera(true)
            }else if(student!!.rapid.trim().isNotBlank() && student!!.rapid.trim().isNotEmpty()){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                    ImageViewer.Builder(activity!!,arrayOf("content://" + student!!.rapid)).setStartPosition(0).show()
                }else{
                    ImageViewer.Builder(activity!!,arrayOf("file://" + student!!.rapid)).setStartPosition(0).show()
                }
            }
        }

        if(student != null){
            if((student!!.rapid.trim().isBlank() && student!!.rapid.trim().isEmpty()) && (student!!.lucid.trim().isBlank() && student!!.lucid.trim().isEmpty())){
                lucidBtn?.visibility = View.GONE
                rapidBtn?.visibility = View.GONE
            }else{
                if(student!!.rapid.trim().isBlank() && student!!.rapid.trim().isEmpty()){
                    rapidBtn?.visibility = View.INVISIBLE
                }
                if(student!!.lucid.trim().isBlank() && student!!.lucid.trim().isEmpty()){
                    lucidBtn?.visibility = View.INVISIBLE
                }
            }
        }

        var interventionsRV = view.findViewById<RecyclerView>(R.id.fragment_student_intervention_RV)
        interventionsRV.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        adapter = ProfileInterventionListAdapter(sessions, activity!!, this)
        interventionsRV.adapter = adapter
        interventionsRV.addItemDecoration(VerticalSpaceItemDecoration())

        var interventionNames: ArrayList<String> = ArrayList()
        val realm = Realm.getDefaultInstance()
        var interventions = realm.where<Intervention>().findAll().sort("name",Sort.DESCENDING)
        for (intervention in interventions) {
            if (!interventionNames.contains(intervention.name)) {
                interventionNames.add(intervention.name)
            }
        }
        if (student != null) {
            val sessions = realm.where<InterventionSession>().equalTo("student.id",student!!.id).findAll()
            for (session in sessions){
                if(session.intervention != null && interventionNames.contains(session.intervention!!.name) && !session.finished){
                    interventionNames.remove(session.intervention!!.name)
                }
            }
        }else{
            menuBtn?.visibility = View.GONE
        }

        addInterventionBtn = com.getbase.floatingactionbutton.FloatingActionButton(activity!!)
        addInterventionBtn?.title = "Add Intervention"
        addInterventionBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.icon)!!)
        addInterventionBtn?.setOnClickListener { v ->
            var spinner = Spinner(context)
            var spinnerAdapter =
                ArrayAdapter(activity!!.applicationContext, R.layout.spinner_item, interventionNames)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item)
            spinner.adapter = spinnerAdapter
            var params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            var margin = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                40F,
                activity!!.applicationContext.resources.displayMetrics
            ).roundToInt()
            params.setMargins(margin, margin, margin, margin)
            spinner.layoutParams = params
            AlertDialog.Builder(activity!!).setTitle("Add Intervention").setView(spinner).setCancelable(true)
                .setPositiveButton("Add") { dialogView, i ->
                    if (spinner.selectedItem != null) {
                        val selectedName: String = interventionNames.get(spinner.selectedItemPosition)
                        val realm = Realm.getDefaultInstance()
                        var intervention = realm.where<Intervention>()
                            .equalTo("name", selectedName)
                            .findFirst()
                        if (intervention != null) {
                            realm.executeTransaction {
                                var session = realm.createObject<InterventionSession>(if (randUUID != "") randUUID else UUID.randomUUID().toString())
                                session.student = student
                                session.intervention = intervention
                                sessions.add(0, session)
                            }
                            adapter?.notifyDataSetChanged()

                        }
                    }

                }.create().show()
        }
        addRapidBtn = com.getbase.floatingactionbutton.FloatingActionButton(activity!!)
        addRapidBtn?.title = "Add Rapid"
        addRapidBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.rp)!!)
        addRapidBtn?.setOnClickListener {
            launchCamera(true)
        }
        addRecallBtn = com.getbase.floatingactionbutton.FloatingActionButton(activity!!)
        addRecallBtn?.title = "Add Recall"
        addRecallBtn?.setIconDrawable(ContextCompat.getDrawable(activity!!,R.drawable.rc)!!)
        addRecallBtn?.setOnClickListener {
            launchCamera(false)
        }

        menuBtn?.addButton(addInterventionBtn)
        menuBtn?.addButton(addRapidBtn)
        menuBtn?.addButton(addRecallBtn)
        return view
    }

    override fun onPause() {
        super.onPause()
        if (student != null) {
            val dateFormat = SimpleDateFormat("dd/MM/yyyy")
            Realm.getDefaultInstance().executeTransaction {

                if (dobET!!.text.toString().trim().isNotEmpty() && student!!.dob != dateFormat.parse(dobET!!.text.toString())) {
                    student!!.dob = dateFormat.parse(dobET!!.text.toString())
                }

                if(lucidUri != null){
                    student?.lucid = lucidUri!!.path
                }

                if(rapidUri != null){
                    student?.rapid = rapidUri!!.path
                }

                if (nameET!!.text.toString().trim().isNotEmpty() && student!!.firstname != nameET!!.text.toString()) {
                    student!!.firstname = nameET!!.text.toString()
                }

                if (lastnameET!!.text.toString().trim().isNotEmpty() && student!!.lastname != lastnameET!!.text.toString()) {
                    student!!.lastname = lastnameET!!.text.toString()
                }

                if (quickDateET!!.text.toString().trim().isNotEmpty() && student!!.quickDate != quickDateET!!.text.toString()) {
                    student!!.quickDate = quickDateET!!.text.toString()
                }

                if (quickPumaET!!.text.toString().trim().isNotEmpty() && student!!.quickPuma != quickPumaET!!.text.toString()) {
                    student!!.quickPuma = quickPumaET!!.text.toString()
                }

                if (quickPiraET!!.text.toString().trim().isNotEmpty() && student!!.quickPira != quickPiraET!!.text.toString()) {
                    student!!.quickPira = quickPiraET!!.text.toString()
                }

                if (quickReadingAgeET!!.text.toString().trim().isNotEmpty() && student!!.quickReadingAge != quickReadingAgeET!!.text.toString()) {
                    student!!.quickReadingAge = quickReadingAgeET!!.text.toString()
                }

                if (quickWritingET!!.text.toString().trim().isNotEmpty() && student!!.quickWriting != quickWritingET!!.text.toString()) {
                    student!!.quickWriting = quickWritingET!!.text.toString()
                }

                if(pupilPremiumCheck!!.isChecked != student!!.pupilPremium){
                    student!!.pupilPremium = pupilPremiumCheck!!.isChecked
                }

            }
        }
    }

    override fun onClick(session: InterventionSession) {

        val bundle = Bundle()
        bundle.putString(Constants.SESSION_ID, session.id)

        var fragment = ProfileInterventionSessionFragment()
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()?.replace(R.id.content_view, fragment)?.addToBackStack(null)?.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.save_menu, menu)
        if(this.student == null){
            menu?.removeItem(R.id.delete)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.save) {

            val realm = Realm.getDefaultInstance()
            val dateFormat = SimpleDateFormat("dd/MM/yyyy")
            if (studentId != null && studentId != "") {
                var student = realm.where<Student>()
                    .equalTo("id", studentId)
                    .findFirst()
                if (student != null) {
                    realm.executeTransaction {
                        if (nameET != null && nameET?.text.toString().trim().isNotEmpty() && nameET?.text.toString().trim().isNotBlank()) {
                            student?.firstname = nameET!!.text.toString()
                        }
                        if (lastnameET != null && lastnameET?.text.toString().trim().isNotEmpty() && lastnameET?.text.toString().trim().isNotBlank()) {
                            student?.lastname = lastnameET!!.text.toString()
                        }
                        if (dobET != null && dobET?.text.toString().trim().isNotEmpty() && dobET?.text.toString().trim().isNotEmpty()) {
                            student?.dob = dateFormat.parse(dobET!!.text.toString())
                        }

                        if(lucidUri != null){
                            student?.lucid = lucidUri!!.path
                        }

                        if(rapidUri != null){
                            student?.rapid = rapidUri!!.path
                        }

                        if (quickDateET!!.text.toString().trim().isNotEmpty() && student!!.quickDate != quickDateET!!.text.toString()) {
                            student!!.quickDate = quickDateET!!.text.toString()
                        }

                        if (quickPumaET!!.text.toString().trim().isNotEmpty() && student!!.quickPuma != quickPumaET!!.text.toString()) {
                            student!!.quickPuma = quickPumaET!!.text.toString()
                        }

                        if (quickPiraET!!.text.toString().trim().isNotEmpty() && student!!.quickPira != quickPiraET!!.text.toString()) {
                            student!!.quickPira = quickPiraET!!.text.toString()
                        }

                        if (quickReadingAgeET!!.text.toString().trim().isNotEmpty() && student!!.quickReadingAge != quickReadingAgeET!!.text.toString()) {
                            student!!.quickReadingAge = quickReadingAgeET!!.text.toString()
                        }

                        if (quickWritingET!!.text.toString().trim().isNotEmpty() && student!!.quickWriting != quickWritingET!!.text.toString()) {
                            student!!.quickWriting = quickWritingET!!.text.toString()
                        }

                        if(pupilPremiumCheck!!.isChecked != student!!.pupilPremium){
                            student!!.pupilPremium = pupilPremiumCheck!!.isChecked
                        }

                        if (sessions.size > 0) {
                            for (session in sessions) {
                                session.student = student
                            }
                        }
                    }
                }
            } else {
                realm.executeTransaction {
                    student = realm.createObject<Student>(UUID.randomUUID().toString())
                    if (nameET != null && nameET?.text.toString().trim().isNotEmpty() && nameET?.text.toString().trim().isNotBlank()) {
                        student?.firstname = nameET!!.text.toString()
                    }
                    if (lastnameET != null && lastnameET?.text.toString().trim().isNotEmpty() && lastnameET?.text.toString().trim().isNotBlank()) {
                        student?.lastname = lastnameET!!.text.toString()
                    }
                    if (dobET != null && dobET?.text.toString().trim().isNotEmpty() && dobET?.text.toString().trim().isNotEmpty()) {
                        student?.dob = dateFormat.parse(dobET!!.text.toString())
                    }

                    if(lucidUri != null){
                        student?.lucid = lucidUri!!.path
                    }

                    if(rapidUri != null){
                        student?.rapid = rapidUri!!.path
                    }

                    if (quickDateET!!.text.toString().trim().isNotEmpty() && student!!.quickDate != quickDateET!!.text.toString()) {
                        student!!.quickDate = quickDateET!!.text.toString()
                    }

                    if (quickPumaET!!.text.toString().trim().isNotEmpty() && student!!.quickPuma != quickPumaET!!.text.toString()) {
                        student!!.quickPuma = quickPumaET!!.text.toString()
                    }

                    if (quickPiraET!!.text.toString().trim().isNotEmpty() && student!!.quickPira != quickPiraET!!.text.toString()) {
                        student!!.quickPira = quickPiraET!!.text.toString()
                    }

                    if (quickReadingAgeET!!.text.toString().trim().isNotEmpty() && student!!.quickReadingAge != quickReadingAgeET!!.text.toString()) {
                        student!!.quickReadingAge = quickReadingAgeET!!.text.toString()
                    }

                    if (quickWritingET!!.text.toString().trim().isNotEmpty() && student!!.quickWriting != quickWritingET!!.text.toString()) {
                        student!!.quickWriting = quickWritingET!!.text.toString()
                    }

                    if(pupilPremiumCheck!!.isChecked != student!!.pupilPremium){
                        student!!.pupilPremium = pupilPremiumCheck!!.isChecked
                    }
                    if (sessions.size > 0) {
                        for (session in sessions) {
                            session.student = student
                        }
                    }
                }
            }
            activity?.supportFragmentManager?.popBackStack()
            return true
        } else if (item?.itemId == R.id.delete) {
            AlertDialog.Builder(ContextThemeWrapper(activity!!,R.style.myDialog))
                .setTitle("Delete Confirmation")
                .setMessage("Are you sure you want to delete this student?")
                .setPositiveButton("Yes") { dialogInterface, i ->

                    if (studentId != "") {
                        val realm = Realm.getDefaultInstance()
                        val student = realm.where<Student>().equalTo("id", studentId).findFirst()
                        if (student != null) {
                            if(student.rapid.isNotEmpty() && student.rapid.isNotBlank()){
                                val file = File(student.rapid)
                                if(file.exists()){
                                    try {
                                        file.delete()
                                    }catch (e:IOException){
                                        e.printStackTrace()
                                    }
                                }
                            }
                            if(student.lucid.isNotEmpty() && student.lucid.isNotBlank()){
                                val file = File(student.lucid)
                                if(file.exists()){
                                    try {
                                        file.delete()
                                    }catch (e:IOException){
                                        e.printStackTrace()
                                    }
                                }
                            }
                            val sessions = realm.where<InterventionSession>().equalTo("student.id", student.id).findAll()
                            if (sessions != null) {
                                realm.executeTransaction {
                                    sessions.deleteAllFromRealm()
                                    student.deleteFromRealm()
                                    this.student = null
                                }
                            } else {
                                realm.executeTransaction {
                                    student.deleteFromRealm()
                                    this.student = null
                                }
                            }
                        }
                    }
                    dialogInterface.dismiss()
                    activity?.supportFragmentManager?.popBackStack()
                }
                .setNegativeButton("No") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    private fun launchCamera(isRapid: Boolean) {

        if(randUUID == ""){
            randUUID = UUID.randomUUID().toString()
        }
        val path = Environment.getExternalStorageDirectory().toString()

        val folder = File(path,"Interventions")

        if(!folder.exists()){
            folder.mkdirs()
        }
        val uri = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            Uri.parse("content://${folder.absolutePath}/${randUUID}_${ if (isRapid) "rapid" else "lucid"}.png")
        }else{
            Uri.parse("file://${folder.absolutePath}/${randUUID}_${ if (isRapid) "rapid" else "lucid"}.png")
        }
        if(isRapid){
            rapidUri = uri
        }else{
            lucidUri = uri
        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri)
        intent.resolveActivity(activity!!.packageManager).also {
            startActivityForResult(intent, if(isRapid) Constants.RAPID_PHOTO_REQUEST else Constants.LUCID_PHOTO_REQUEST)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == Constants.RAPID_PHOTO_REQUEST && resultCode == Activity.RESULT_OK){

            Realm.getDefaultInstance().executeTransaction {
                if(student != null){
                    student!!.rapid = rapidUri!!.path
                }
            }
            rapidBtn?.visibility = View.VISIBLE
        }else if(requestCode == Constants.LUCID_PHOTO_REQUEST && resultCode == Activity.RESULT_OK){
            Realm.getDefaultInstance().executeTransaction {
                if(student != null){
                    student!!.lucid = lucidUri!!.path
                }
            }
            lucidBtn?.visibility = View.VISIBLE
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}