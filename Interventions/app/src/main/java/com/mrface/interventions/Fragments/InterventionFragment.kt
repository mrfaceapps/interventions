package com.mrface.interventions.Fragments

import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.widget.*
import com.getbase.floatingactionbutton.AddFloatingActionButton
import com.mrface.interventions.Activities.MainActivity
import com.mrface.interventions.R
import com.mrface.interventions.Utils.Constants
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.px
import com.mrface.interventions.adapters.InterventionStudentsAdapter
import com.mrface.interventions.models.Intervention
import com.mrface.interventions.models.InterventionSession
import com.mrface.interventions.models.Student
import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class InterventionFragment : Fragment(), StudentItemClickListener {

    var interventionId: String = ""
    var studentId: String = ""
    var students: ArrayList<Student> = ArrayList()
    var studentAdapter: InterventionStudentsAdapter? = null
    var activeState: Boolean = true
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val main = activity as? MainActivity
        if(main != null){
            main.supportActionBar?.setHomeButtonEnabled(true)
            main.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            main.supportActionBar?.setDisplayShowHomeEnabled(true)
        }
        if(arguments != null && arguments!!.containsKey(Constants.ACTIVE_STUDENT_FLAG)){
            activeState = arguments!!.getBoolean(Constants.ACTIVE_STUDENT_FLAG,true)
        }
        setHasOptionsMenu(true)
        val view: View = inflater.inflate(R.layout.fragment_list, null)
        var intervention: Intervention? = null

        val border = GradientDrawable()
        border.cornerRadius = 20.px.toFloat()
        border.setColor(ContextCompat.getColor(activity!!.applicationContext,R.color.LightGrey))

        if (arguments != null && arguments!!.containsKey(Constants.INTERVENTION_ID)) {
            interventionId = arguments!!.getString(Constants.INTERVENTION_ID)!!
            intervention = Realm.getDefaultInstance().where<Intervention>().equalTo("id", interventionId).findFirst()
            if(intervention != null){
                main?.supportActionBar?.title = intervention?.name
                main?.supportActionBar?.setBackgroundDrawable(ColorDrawable(resources.getIntArray(R.array.colors)[intervention!!.colorIndex]))
                border.setStroke(5.px,activity!!.applicationContext.resources.getIntArray(R.array.colors)[intervention!!.colorIndex])
            }
        } else {
            main?.supportActionBar?.title = "Students"
        }

        obtainStudents()

        var studentList: RecyclerView = view.findViewById(R.id.fragment_list_RV)
        studentList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        studentAdapter = InterventionStudentsAdapter(students, activity!!.applicationContext, this)
        studentList.adapter = studentAdapter

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
            studentList.setBackgroundDrawable(border)
        }else{
            studentList.background = border
        }


        val realm = Realm.getDefaultInstance()
        var allStudentList: ArrayList<Student> = ArrayList(realm.where<Student>().findAll().sort("lastname"))

        for(tmp in students){
            allStudentList.remove(tmp)
        }
        var studentNames: ArrayList<String> = ArrayList()
        for(tmp in allStudentList){
            studentNames.add(tmp.firstname + " " + tmp.lastname)
        }

        view.findViewById<AddFloatingActionButton>(R.id.fragment_add_btn).setOnClickListener { v ->
            var spinner = Spinner(context)
            var spinnerAdapter = ArrayAdapter(activity!!.applicationContext,R.layout.spinner_item,studentNames)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item)
            spinner.adapter = spinnerAdapter
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            var margin = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40F,activity!!.applicationContext.resources.displayMetrics).roundToInt()
            params.setMargins(margin,margin,margin,margin)
            spinner.layoutParams = params
            AlertDialog.Builder(activity!!).setTitle("Add Student").setView(spinner).setCancelable(true).setPositiveButton("Add") { dialogView, i ->
                if(spinner.selectedItem != null){
                    if(intervention != null){
                        realm.executeTransaction {
                            var session = realm.createObject<InterventionSession>(UUID.randomUUID().toString())
                            session.student = allStudentList.get(spinner.selectedItemPosition)
                            session.intervention = intervention
                            studentNames.removeAt(spinner.selectedItemPosition)
                            allStudentList.removeAt(spinner.selectedItemPosition)
                        }

                        obtainStudents()
                    }
                }

            }.create().show()
        }
        return view
    }

    fun obtainStudents() {
        val realm = Realm.getDefaultInstance()
        val results = realm.where<InterventionSession>()
            .equalTo("intervention.id", interventionId)
            .notEqualTo("finished",activeState)
            .findAll().sort("startDate",Sort.DESCENDING)

        students.clear()
        var tmpStudents = ArrayList<Student>()
        for (session: InterventionSession in results) {
            if (session.student != null && !tmpStudents.contains(session.student!!)) {
                tmpStudents.add(session.student!!)
            }
        }

        students = ArrayList(tmpStudents.sortedBy { it.lastname })
        Log.d("InterventionFragment","Students: " + students)
        studentAdapter?.notifyDataSetChanged()
        Log.d("InterventionFragment","adapter: " + studentAdapter?.items)
    }

    override fun onClick(item: Student) {
        val bundle = Bundle()
        bundle.putString(Constants.STUDENT_ID,item.id)

        var fragment = StudentProfileFragment()
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()?.replace(R.id.content_view,fragment)?.addToBackStack(null)?.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.save_menu,menu)
        menu?.removeItem(R.id.save)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == R.id.delete){
            AlertDialog.Builder(ContextThemeWrapper(activity!!,R.style.myDialog))
                .setTitle("Delete Confirmation")
                .setMessage("Are you sure you want to delete this intervention?")
                .setPositiveButton("Yes") { dialogInterface, i ->

                    if(interventionId != ""){
                        val realm = Realm.getDefaultInstance()
                        val intervention = realm.where<Intervention>().equalTo("id",interventionId).findFirst()
                        if(intervention != null){
                            realm.executeTransaction {
                                if(activeState){
                                    intervention.deletedActive = true
                                }else{
                                    intervention.deletedInactive = true
                                }
                            }
                        }
                    }

                    dialogInterface.dismiss()
                    activity?.supportFragmentManager?.popBackStack()
                }
                .setNegativeButton("No") { dialogInterface, i ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
            return true
        }else{
            return super.onOptionsItemSelected(item)
        }
    }
}