package com.mrface.interventions.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mrface.interventions.R
import com.mrface.interventions.Utils.StudentItemClickListener
import com.mrface.interventions.Utils.listen
import com.mrface.interventions.models.Student
import kotlinx.android.synthetic.main.rv_double.view.*

class InterventionStudentsAdapter(val items: ArrayList<Student>, val context: Context, val listener: StudentItemClickListener) : RecyclerView.Adapter<StudentsViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): StudentsViewHolder {
       return StudentsViewHolder(LayoutInflater.from(context).inflate(R.layout.rv_double,p0,false)).listen { pos, type->
           listener.onClick(items.get(pos))
       }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: StudentsViewHolder, p1: Int) {
        p0.double1.text = items.get(p1).firstname + " " + items.get(p1).lastname
    }

}
class StudentsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val double1 = view.rv_double_1
    val double2 = view.rv_double_2
}